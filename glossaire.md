WebSocket : WebSockets est une technologie qui permet d'ouvrir un canal de communication full-duplex sur un socket TCP entre le navigateur de l'utilisateur et un serveur. Avec l'API en JavaScript, il est possible d'envoyer des messages à un serveur et recevoir des réponses en fonction des événements sans avoir à interroger le serveur pour une réponse.

Server-sent events : possibilité pour un serveur d'envoyer de nouvelles données à une page Web à tout moment, en "poussant" les messages à la page web. Ces messages entrants peuvent être traités comme des événements côté client (traitement en JavaScript par exemple).

Full-duplex : terme utilisé pour désigner un canal de communication bidirectionnel où l'information est transportée simultanément dans les deux sens (client/serveur ; serveur/client).

## Pour cahier des charges

- Soutien (métier et technique) : Personne qui se charge de prendre le relais quand le SAV (service après vente) ne peut pas résoudre un problème. Niveau au dessus des personnes du SAV au niveau des accès, droits etc. Un soutien métier est plus au niveau fonctionnel et se charge plutot des applications du SI, alors que le soutien technique est plus un technicien ayant les compétences pour résoudre les problèmes d'un point de vue technique.

- Workflow : Représentation d'une suite de tâches ou opérations effectuées par une personne, un groupe de personnes, un organisme, etc. Le terme flow (flux) renvoie au passage du produit, du document, de l'information, etc., d'une étape à l'autre. Il représente donc un historique d'états pour avoir un suivi complet.

- RSC : Responsable Service Client

- e-buro : Propre à Orange, c'est une version de windows modifiée pour avoir des caractéristiques supplémentaires et mieux correspondre au fonctionnement interne de l'entreprise, destiné à l'ensemble du personnel.

- infocentre ("tableau de bord à l’usage des soutiens, des SIE/SLM/MOA/HelpDesk") : Ici, c'est un ensemble de grosses bases de données ("entrepôt de données") mise à disposition de certains services chez Orange permettant la récupération de différentes informations tel que des références techniques d'équipements, de clients, etc.

http://www.developpez.net/forums/d29666/emploi-etudes-informatique/etudes/difference-entre-moe-moa/